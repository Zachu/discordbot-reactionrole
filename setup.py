from setuptools import find_namespace_packages, setup

tests_require = [
    'pylint>=2.6.0',
    'flake8>=3.8.4',
]
install_requires = [
    'discord.py>=1.5.0',
    'sqlalchemy>=1.3.20',
    'alembic>=1.4.3',
]

setup(
    name='discordbot-reactionrole',
    package_dir={'': 'src'},
    packages=find_namespace_packages('src', include=['discordbot.*']),

    url='https://gitlab.utopiaanalytics.com/Zachu/discordbot-reactionrole',
    author='Jani Korhonen',
    author_email='jani@zachu.fi',
    description='Lets see',

    install_requires=install_requires,
    tests_require=tests_require,

    extras_require={
        'tests': tests_require,
    },
)
