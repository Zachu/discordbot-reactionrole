def parse_message_link(link):
    ids = link.split('/')[4:]
    if any(not id.isdigit() for id in ids):
        raise ValueError
    guild, channel, msg_id = ids
    return guild, channel, msg_id
