import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class TrackMessageReaction(Base):
    __tablename__ = 'reactionrole_track'

    id = sa.Column('id', sa.Integer, primary_key=True)
    guild_id = sa.Column('guild_id', sa.String(length=32))
    channel_id = sa.Column('channel_id', sa.String(length=32))
    message_id = sa.Column('message_id', sa.String(length=32))
    role_id = sa.Column('role_id', sa.String(length=32))
    emoji = sa.Column('emoji', sa.Unicode(length=1))

    def __repr__(self):
        return (
            '<{classname}('
            "guild_id='{guild_id}', "
            "channel_id='{channel_id}', "
            "message_id='{message_id}', "
            "role_id='{role_id}', "
            "emoji='{emoji}'"
            ')>'.format(
                classname=self.__class__.__name__,
                guild_id=self.guild_id,
                channel_id=self.channel_id,
                message_id=self.message_id,
                role_id=self.role_id,
                emoji=self.emoji,
            )
        )

    def add(self, db):
        try:
            db.add(self)
            db.commit()
        except Exception:
            db.rollback()
            raise

    def remove(self, db):
        try:
            db.delete(self)
            db.commit()
        except Exception:
            db.rollback()
            raise


    @staticmethod
    def all(db):
        return db.query(TrackMessageReaction).all()
