"""
Track messages for reactions and assign roles accordingly.

NOTE: The bot role needs to be above the roles it manages.
Needed permissions (268504064):
- General Permissions
  - Manage Roles
  - View Channels
- Text Permissions
  - Send Messages
  - Read Message History

TODO:
  - Command channels and other authorization stuff
  - Handle TODO's
  - Go through the code and see it's reasonable
"""
import asyncio
import logging
import re
import os

import discord
import discord.ext.commands
import discord.ext.commands.errors
from sqlalchemy.exc import IntegrityError, OperationalError

from . import utils, models


class ReactionRole(discord.ext.commands.Cog):
    tracks = []
    ready = False

    def __init__(self, bot):
        self.bot = bot
        self.tracks = []
        self.logger = logging.getLogger(__name__)
        self._persistence = False

        if self.bot.db:
            try:
                self.tracks = models.TrackMessageReaction.all(self.bot.db)
                self._persistence = True
            except OperationalError as exc:
                if str(exc.orig) == 'no such table: {}'.format(models.TrackMessageReaction.__tablename__):
                    self.logger.warning('Dropping persistence. Table not found in database. Maybe run migrations?')
                else:
                    raise
        else:
            self.logger.warning('Dropping persistence. No database connection provided.')

    # Methods

    def get_tracking(self, guild_id=None, channel_id=None, message_id=None, role_id=None, emoji=None):
        output = []
        for track in self.tracks:
            # pylint: disable=too-many-boolean-expressions  # or is there a cleaner way?
            if (
                (guild_id and str(track.guild_id) != str(guild_id)) or
                (channel_id and str(track.channel_id) != str(channel_id)) or
                (message_id and str(track.message_id) != str(message_id)) or
                (role_id and str(track.role_id) != str(role_id)) or
                (emoji and str(track.emoji) != str(emoji))
            ):
                continue
            output.append(track)

        return output

    def _add_tracking(self, guild_id, channel_id, message_id, role_id, emoji):
        track = models.TrackMessageReaction(
            guild_id=guild_id,
            channel_id=channel_id,
            message_id=message_id,
            role_id=role_id,
            emoji=emoji,
        )
        if track in self.tracks:
            return

        self.logger.info('Adding track %s', track)
        self.tracks.append(track)
        if self._persistence:
            track.add(self.bot.db)

        return track

    async def _add_role(self, role, member, **kwargs):
        """Add role for member."""
        if member not in role.members:
            self.logger.info('Adding role %s for user %s. Reason: %s', role, member, kwargs.get('reason', None))
            await member.add_roles(role, **kwargs)

    async def _del_role(self, role, member, **kwargs):
        """Remove role from member."""
        if member in role.members:
            self.logger.info('Removing role %s from user %s. Reason: %s', role, member, kwargs.get('reason', None))
            await member.remove_roles(role, **kwargs)

    def _get_guild_from_context(self, ctx):
        guild = None
        if hasattr(ctx, 'guild'):
            guild = ctx.guild
        elif hasattr(ctx.member, 'guild'):
            guild = ctx.member.guild
        elif hasattr(ctx, 'guild_id'):
            guild = self.bot.get_guild(ctx.guild_id)
        return guild

    async def _get_link_message(self, ctx, link):
        # Validate link
        try:
            guild_id, channel_id, msg_id = utils.parse_message_link(link)
        except ValueError:
            await ctx.send('Link does not look like a message link.')
            return

        # Validate guild
        guild = ctx.message.guild
        if guild_id != str(guild.id):
            await ctx.send('Link is not for this guild.')
            return

        # Validate channel
        channel = ctx.message.guild.get_channel(int(channel_id))
        if not channel:
            await ctx.send('Link is not for this guild or I can not see the channel.')
            return

        # Fetch message
        try:
            message = await channel.fetch_message(msg_id)
        except discord.HTTPException as exc:
            if exc.code == 10008:  # Unknown message
                await ctx.send('Unable to find message {message} from channel {channel}'.format(
                    message=msg_id,
                    channel=channel.mention(),
                ))
            else:
                await ctx.send('Unknown error when fetching message')
                print(type(exc).__name__ + ': ' + str(exc))
            return

        return message

    async def _get_role_from_mention(self, ctx, mention):
        match = re.match(r'<@&(\d+)>', mention)
        if not match:
            await ctx.send('This does not look like role mention: "{}"'.format(mention))
            return
        return ctx.guild.get_role(int(match.groups()[0]))

    async def _embed_track_message(self, ctx, track):
        channel = ctx.guild.get_channel(int(track.channel_id))
        role = ctx.guild.get_role(int(track.role_id))
        message = await channel.fetch_message(int(track.message_id))
        return dict(
            content=message.jump_url,
            embed=discord.Embed(
                title='Tracking reaction {emoji} for role @{role}'.format(emoji=track.emoji, role=role.name),
                description=message.clean_content,
                timestamp=message.edited_at if message.edited_at else message.created_at,
                colour=role.colour,
            ),
        )

    async def _get_track_role_members(self, tracks):
        # Get all tracked messages
        tasks = []
        for t_guild_id, t_channel_id, t_message_id in {(t.guild_id, t.channel_id, t.message_id) for t in tracks}:
            guild = self.bot.get_guild(int(t_guild_id))
            channel = guild.get_channel(int(t_channel_id))
            tasks.append(channel.fetch_message(int(t_message_id)))
        messages = await asyncio.gather(*tasks)

        # Get all reacted users by roles
        role_tasks = {}
        for message in messages:
            emojis = [t.emoji for t in tracks if t.message_id == str(message.id)]
            for reaction in message.reactions:
                if reaction.emoji not in emojis:
                    continue
                role = next(t.role_id for t in tracks if t.message_id == str(message.id) and t.emoji == reaction.emoji)
                role_tasks.setdefault(role, []).append(reaction.users().flatten())

        roles = {}
        for role, tasks in role_tasks.items():
            users = await asyncio.gather(*tasks)
            roles[role] = {user.id for sublist in users for user in sublist}

        return roles

    # Listeners

    @discord.ext.commands.Cog.listener()
    async def on_ready(self):
        # Adjust tracked roles as they should be
        tasks = []
        for guild_id in {track.guild_id for track in self.tracks}:
            guild = self.bot.get_guild(int(guild_id))
            role_members = await self._get_track_role_members(self.get_tracking(guild_id=guild_id))

            # Remove roles (if some was added when bot wasn't ready)
            for role_id in {t.role_id for t in self.get_tracking(guild_id=guild_id)}:
                role = guild.get_role(int(role_id))
                for member in role.members:
                    if member.id not in role_members.get(role_id, []):
                        tasks.append(self._del_role(role=role, member=member, reason='On_ready check'))

            # Add roles (if some was missing when bot wasn't ready)
            for role_id, member_ids in role_members.items():
                role = guild.get_role(int(role_id))
                missing = member_ids.difference({m.id for m in role.members})
                for member_id in missing:
                    member = guild.get_member(member_id)
                    tasks.append(self._add_role(role=role, member=member, reason='On_ready check'))

        await asyncio.gather(*tasks)

    @discord.ext.commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.errors.UserInputError):
            await ctx.send((
                '**Error**: {error}'
                '```\n'
                '{prefix}{command} {signature}\n'
                '\n'
                '{help}\n'
                '```'
            ).format(
                error=str(error),
                prefix=self.bot.command_prefix,
                command=ctx.command.qualified_name,
                signature=ctx.command.signature,
                help=ctx.command.help,
            ))
        else:
            await ctx.send('**Error**: Unknown error happened')
            raise error

    @discord.ext.commands.Cog.listener()
    async def on_raw_reaction_add(self, ctx):
        """Add role for user if they correctly react to tracked message"""
        tracks = self.get_tracking(
            guild_id=ctx.guild_id,
            channel_id=ctx.channel_id,
            message_id=ctx.message_id,
            emoji=ctx.emoji.name
        )
        for track in tracks:
            await self._add_role(
                role=ctx.member.guild.get_role(int(track.role_id)),
                member=ctx.member,
                reason='Reaction {} added for message {}'.format(ctx.emoji.name, ctx.message_id)
            )

    @discord.ext.commands.Cog.listener()
    async def on_raw_reaction_remove(self, ctx):
        """Remove a role from user if they no longer have any tracked reactions for that role"""
        track = self.get_tracking(
            guild_id=ctx.guild_id,
            channel_id=ctx.channel_id,
            message_id=ctx.message_id,
            emoji=ctx.emoji.name
        )
        if not track:
            return

        # Check if user has emojis in other messages that track the same role
        role_id = track[0].role_id
        guild = self.bot.get_guild(int(track[0].guild_id))
        other_tracks = self.get_tracking(guild_id=ctx.guild_id, role_id=role_id)
        tasks = []

        self.logger.debug(
            'User %s removed reaction %s from %s. Fetching other messages tracking role %s.',
            ctx.user_id,
            ctx.emoji.name,
            ctx.message_id,
            role_id,
        )
        for channel_id, message_id in {(t.channel_id, t.message_id) for t in other_tracks}:
            channel = guild.get_channel(int(channel_id))
            tasks.append(channel.fetch_message(int(message_id)))
        messages = await asyncio.gather(*tasks)

        tasks = []
        for message in messages:
            emojis = [t.emoji for t in other_tracks if t.message_id == str(message.id)]
            for reaction in message.reactions:
                if reaction.emoji not in emojis:
                    continue
                tasks.append(reaction.users().get(id=ctx.user_id))

        users = await asyncio.gather(*tasks)
        if any([u is not None for u in users]):
            self.logger.debug('User %s still attached to some messages. Not removing role', ctx.user_id)
        else:
            role = guild.get_role(int(role_id))
            member = guild.get_member(ctx.user_id)
            await self._del_role(
                role=role,
                member=member,
                reason='Reaction {} removed from message {}'.format(ctx.emoji.name, ctx.message_id)
            )

    # Commands

    @discord.ext.commands.group()
    async def reactionrole(self, ctx):
        """Track messages for reactions and assign roles accordingly."""
        pass

    @reactionrole.command(name='list')
    async def _list(self, ctx):
        """List all tracked messages on this guild."""
        tracks = self.get_tracking(ctx.message.guild.id)
        if not tracks:
            await ctx.send('No messages being tracked.')
            return

        tasks = []
        for track in tracks:
            tasks.append(self._embed_track_message(ctx, track))
        messages = await asyncio.gather(*tasks)

        for message in messages:
            message['content'] = 'Tracking message\n' + message['content']
            await ctx.send(**message)

    @reactionrole.command()
    async def add(self, ctx, msg, role, emoji):
        """Add reaction tracking for messages to give roles accordingly."""
        # TODO: document parameters
        # TODO: Validate reaction somehow
        message = await self._get_link_message(ctx, msg)
        role = await self._get_role_from_mention(ctx, role)

        # Add tracking
        track = self._add_tracking(
            guild_id=ctx.guild.id,
            channel_id=message.channel.id,
            message_id=message.id,
            role_id=role.id,
            emoji=emoji,
        )
        message = await self._embed_track_message(ctx, track)
        if track:
            message['content'] = 'Tracking added\n' + message['content']
        else:
            message['content'] = 'Could not add tracking. Probably already exists?\n' + message['content']
        await ctx.send(**message)

    @reactionrole.command(name='del')
    async def _del(self, ctx, msg, role, emoji):
        # TODO: document parameters
        # TODO: Validate reaction somehow
        message = await self._get_link_message(ctx, msg)
        role = await self._get_role_from_mention(ctx, role)

        tracks = self.get_tracking(
            guild_id=ctx.guild.id,
            channel_id=message.channel.id,
            message_id=message.id,
            role_id=role.id,
            emoji=emoji,
        )

        # There should be only one element in tracks
        for track in tracks:
            self.logger.info('Removing track %s', track)
            track.remove(self.bot.db)
            self.tracks.remove(track)
            message = await self._embed_track_message(ctx, track)
            message['content'] = 'Tracking removed\n' + message['content']
            await ctx.send(**message)


def setup(bot):
    """Initialize extension"""
    bot.add_cog(ReactionRole(bot))


def migrations():
    """Return path for migrations"""
    return os.path.join(os.path.dirname(__file__), 'migrations')
