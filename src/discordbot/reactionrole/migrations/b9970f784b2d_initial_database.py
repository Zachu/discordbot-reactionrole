"""Initial database

Revision ID: b9970f784b2d
Revises:
Create Date: 2020-10-25 19:42:33.508604+00:00

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = 'b9970f784b2d'
down_revision = None
branch_labels = None
depends_on = None
table = 'reactionrole_track'


# pylint: disable=no-member  # Yes there is!
def upgrade():
    op.create_table(
        table,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('guild_id', sa.String(length=32)),
        sa.Column('channel_id', sa.String(length=32)),
        sa.Column('message_id', sa.String(length=32)),
        sa.Column('emoji', sa.Unicode(length=4), nullable=True),
        sa.Column('role_id', sa.String(length=32)),
    )
    op.create_index('idx_' + table + '_guild_channel_id_message_emoji_role', table,
                    ['guild_id', 'channel_id', 'message_id', 'emoji', 'role_id'], unique=True)


# pylint: disable=no-member  # Yes there is!
def downgrade():
    op.drop_index('idx_' + table + '_guild_channel_id_message_emoji_role', table)
    op.drop_table(table)
